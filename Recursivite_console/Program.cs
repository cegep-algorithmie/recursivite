﻿using System;
using System.Collections.Generic;
using System.Reflection;
using Excel = Microsoft.Office.Interop.Excel;
using System.Diagnostics;
using System.Runtime.InteropServices;
using Recursivite_Bibliotheque;
using System.IO;

namespace Recursivite_console
{
    class Program
    {
        static void Main(string[] args)
        {
            #region "Compter en négatif"
            // En ordre croissant:
            Console.Out.WriteLine("Compter en négatif en ordre croissant:");
            FonctionsRecursives.AfficherNombresNegatifsCroissant(0);
            Console.Out.WriteLine("--");
            FonctionsRecursives.AfficherNombresNegatifsCroissant(-8);
            Console.Out.WriteLine("--------------------------------------");
            //En ordre décroissant
            Console.Out.WriteLine("Compter en négatif en ordre décroissant:");
            FonctionsRecursives.AfficherNombresNégatifsDecroissant(0);
            Console.Out.WriteLine("--");
            FonctionsRecursives.AfficherNombresNégatifsDecroissant(-8);
            Console.Out.WriteLine("--------------------------------------");
            #endregion

            #region "Division entière"
            Console.Out.WriteLine("Calcul du reste de la division entière de deux nombres entiers sans utiliser le modulo:");
            Console.Out.WriteLine(FonctionsRecursives.CalculerResteDivisionEntiere(25, 3));
            Console.Out.WriteLine("--");
            Console.Out.WriteLine(FonctionsRecursives.CalculerResteDivisionEntiere(99, 2));
            Console.Out.WriteLine("--");
            Console.Out.WriteLine(FonctionsRecursives.CalculerResteDivisionEntiere(17, 3));
            Console.Out.WriteLine("--------------------------------------");
            #endregion

            #region "Suite de Fibonacci"
            // Calculez Fibonacci de 10 et validez que vous obtenez bien la valeur 55.
            Console.Out.WriteLine("Suite de Fibonacci: Calculez Fibonacci de 10 et validez que vous obtenez bien la valeur 55");
            Console.Out.WriteLine(FonctionsRecursives.CalculerValeurSuiteFibonacci(0));
            Console.Out.WriteLine(FonctionsRecursives.CalculerValeurSuiteFibonacci(1));
            Console.Out.WriteLine(FonctionsRecursives.CalculerValeurSuiteFibonacci(10));
            Console.Out.WriteLine(FonctionsRecursives.CalculerValeurSuiteFibonacci(3));
            Console.Out.WriteLine("--------------------------------------");

            /* Question 3 : modifiez votre programme principal pour qu'il calcule les valeurs de Fibonacci de 1 à 40.
             * Affichez les temps en Ticks et tracez la courbe sur Excel afin de valider la complexité que vous aviez trouvé. */

            Console.Out.WriteLine("Suite de Fibonacci: Calcule les valeurs de Fibonacci de 1 à 40");

            List<Tuple<int, Stopwatch>> listeTemps = new List<Tuple<int, Stopwatch>>();

            for (int nombre = 1; nombre <= 40; nombre++)
            {
                Stopwatch temps = Stopwatch.StartNew();

                Console.Out.WriteLine("Nombre: {0} , valeur: {1}", nombre, FonctionsRecursives.CalculerValeurSuiteFibonacci(nombre));

                temps.Stop();
                listeTemps.Add(new Tuple<int, Stopwatch>(item1: nombre, item2: temps));

                Console.Out.WriteLine(temps.ElapsedTicks);
            }

            string feuilleCalculComplexiteFibonacci = "complexite-fibonacci.xlsx";
            CreerFeuilleCalculExcel(listeTemps, feuilleCalculComplexiteFibonacci);

            Console.Out.WriteLine("--------------------------------------");

            // Question 4 : comment pourriez-vous améliorez votre fonction? Réécrivez une version optimisée.

            Console.Out.WriteLine("Amelioration Suite de Fibonacci: Calcule les valeurs de Fibonacci de 1 à 40");

            List<Tuple<int, Stopwatch>> listeTempsAmelioree = new List<Tuple<int, Stopwatch>>();

            for (int nombre = 1; nombre <= 40; nombre++)
            {
                Stopwatch temps = Stopwatch.StartNew();

                Console.Out.WriteLine("Nombre: {0} , valeur: {1}", nombre, FonctionsRecursives.CalculerValeurSuiteFibonacciAmeliorer(nombre));

                temps.Stop();
                listeTempsAmelioree.Add(new Tuple<int, Stopwatch>(item1: nombre, item2: temps));

                Console.Out.WriteLine(temps.ElapsedTicks);
            }

            string feuilleCalculComplexiteFibonacciAmelioree = "complexite-fibonacci-amelioree.xlsx";
            CreerFeuilleCalculExcel(listeTempsAmelioree, feuilleCalculComplexiteFibonacciAmelioree);

            //Console.Out.WriteLine("--------------------------------------");
            #endregion

            #region "Liste chaînée"
            int[] collection = new int[] { 1, 2, 3, 4, 5, 6, 7, 800, 90, 10 };
            LinkedList<int> listeChainee = new LinkedList<int>(collection);

            /* Question 1 : écrivez une fonction récursive qui prend une liste chaînée générique et qui renvoie son nombre d'éléments
            en parcourant tous ses noeuds. */
            Console.Out.WriteLine("Liste chainée: Nombre d'éléments");
            Console.Out.WriteLine(FonctionsRecursives.ObtenirNombreElementsListeChainee(listeChainee));
            Console.Out.WriteLine("--------------------------------------");

            // Question 2 : écrivez une fonction récursive qui prend une liste chaînée générique et qui renvoie la valeur la plus grande.
            Console.Out.WriteLine("Liste chainée: Valeur la plus grande");
            Console.Out.WriteLine(FonctionsRecursives.ObtenirValeurPlusGrandeListeChainee(listeChainee));
            Console.Out.WriteLine("--------------------------------------");

            /* Question 3 : écrivez une fonction récursive qui prend une liste chaînée générique et une fonction lambda qui ne renvoie rien(Action<TypeElement>)
            et qui applique la fonction lambda sur chacun des noeuds. */
            Console.Out.WriteLine("Liste chainée: Lambda parcours");
            Action<int> action = entier => Console.Out.WriteLine(entier);
            FonctionsRecursives.AppliquerLambdaListeChainee(listeChainee, action);
            Console.Out.WriteLine("--------------------------------------");

            // Question 4 : réécrivez une nouvelle version des fonctions NombreDElements et Maximum en utilisant la fonction de parcours.
            Console.Out.WriteLine("Liste chainée: Lambda nombre d'éléments");
            Console.Out.WriteLine(FonctionsRecursives.ObtenirNombreElementsListeChaineeParcours(listeChainee));
            Console.Out.WriteLine("--------------------------------------");

            Console.Out.WriteLine("Liste chainée: Lambda Valeur la plus grande");
            Console.Out.WriteLine(FonctionsRecursives.ObtenirValeurPlusGrandeListeChaineeParcours(listeChainee));
            Console.Out.WriteLine("--------------------------------------");
            #endregion

            #region "Recherche"
            IEnumerable<int> collectionEnumerable = collection;

            /* Question 1 : écrivez une fonction de recherche récursive qui prend un "IEnumerable" comme collection, une fonction lambda en paramètres 
            et qui renvoie l'élément s'il est trouvé, la valeur par défaut sinon :

            - La fonction lambda prend une valeur en paramètres et renvoie vrai si c'est la valeur à trouver, faux sinon.
            - Votre fonction privée doit prendre un "IEnumerator" en paramètres ainsi que la fonction lambda. */
            Console.Out.WriteLine("IEnumerable: Lambda recherche");
            Func<int, int, bool> rechercheLambda = (valeurComparee, valeurRecherchee) => valeurComparee == valeurRecherchee ? true : default;
            int valeurRecherchee = 800;
            Console.Out.WriteLine(FonctionsRecursives.RechercherElementLambda(collectionEnumerable, valeurRecherchee, rechercheLambda));
            Console.Out.WriteLine("--------------------------------------");

            /* Question 2 : écrivez une fonction récursive qui permet de compter le nombre d'éléments qui correspondent au critère d'une fonction lambda passée en paramètres :

            - La fonction lambda prend une valeur en paramètres et renvoie vrai si c'est la valeur à trouver, faux sinon.
            - Votre fonction privée doit prendre un "IEnumerator" en paramètres ainsi que la fonction lambda.
            - Votre fonction renvoie le nombre d'occurrences */

            Console.Out.WriteLine("IEnumerable: Lambda recherche occurences");
            Func<int, int, bool> rechercheLambdaNombreElements = (valeurComparee, valeurRecherchee) => valeurComparee == valeurRecherchee ? true : false;
            Console.Out.WriteLine(FonctionsRecursives.RechercherNombreElementsLambda(collection, valeurRecherchee, rechercheLambdaNombreElements));
            Console.Out.WriteLine("--------------------------------------");

            /* Question 3 : écrivez une fonction récursive qui permet de filtrer une collection d'éléments qui correspondent au critère d'une fonction lambda passée en paramètres :

            - La fonction lambda prend une valeur en paramètres et renvoie vrai si c'est la valeur à trouver, faux sinon.
            - Votre fonction privée doit prendre un "IEnumerator" en paramètres ainsi que la fonction lambda.
            - Votre fonction renvoie une liste d'éléments du même type que les éléments de l'énumérateur. */

            Console.Out.WriteLine("IEnumerable: Lambda filtrer éléments");
            Func<int, bool> filtreLambda = x => x < 10 ? true : false;
            List<int> liste = FonctionsRecursives.FiltrerElementsLambda(collection, filtreLambda);
            Console.Out.WriteLine(String.Join(", ", liste));
            Console.Out.WriteLine("--------------------------------------");
            #endregion

            #region "Recherche dichotomique"
            /* Écrivez une fonction de recherche dichotomique récursive qui prend un tableau d'éléments,
            une fonction lambda en paramètres et qui renvoie l'élément s'il est trouvé, la valeur par défaut sinon :

            La fonction lambda prend une valeur en paramètres et renvoie un entier correspondant à sa position par rapport à la valeur cherchée.
            Votre fonction privée doit prendre un tableau, les indices de début et de fin du sous-tableau ainsi que la fonction lambda. */
            Console.Out.WriteLine("Recherche Dichotomique:");
            int[] tableauTrie = new int[] { 1, 2, 3, 4, 5, 6, 7, 8, 10, 11 };
            int valeurCherchee = 4;
            Func<int, int> fonctionLambdaDichotomique = valeurComparee =>
            {
                return valeurComparee == valeurCherchee ? 0 : valeurComparee < valeurCherchee ? -1 : 1;
            };
            Console.Out.WriteLine(FonctionsRecursives.RechercheDichotomiqueLambda(tableauTrie, fonctionLambdaDichotomique));
            Console.Out.WriteLine("--------------------------------------");
            #endregion
        }

        #region "Excel"
        public static void CreerFeuilleCalculExcel(List<Tuple<int, Stopwatch>> p_listeTemps, string p_nomFichier)
        {
            string dossierSolution = Directory.GetParent(Environment.CurrentDirectory)?.Parent?.Parent?.Parent?.FullName;
            string cheminAbsoluFichier = dossierSolution + "\\" + p_nomFichier;

            Excel.Application excel = new Excel.Application();

            if (excel != null)
            {
                excel.Visible = true;
                Excel._Workbook classeur;
                classeur = (Excel._Workbook)(excel.Workbooks.Add(Missing.Value));
                Excel._Worksheet feuilleCalcul = (Excel._Worksheet)classeur.ActiveSheet;

                feuilleCalcul.Cells[1, 5] = "Complexité d'une fonction récursive de la suite de Fibonacci";
                feuilleCalcul.Cells[3, 1] = "Nombre";
                feuilleCalcul.Cells[3, 2] = "Temps en Ticks";

                feuilleCalcul.get_Range("A1", "E3").Font.Bold = true;
                feuilleCalcul.get_Range("E1").Font.Size = 20;
                feuilleCalcul.get_Range("A3", "B43").EntireColumn.AutoFit();
                feuilleCalcul.get_Range("A3", "B43").VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                feuilleCalcul.get_Range("A3", "B43").HorizontalAlignment = Excel.XlHAlign.xlHAlignCenter;
                feuilleCalcul.get_Range("A3", "B43").Borders.Weight = Excel.XlBorderWeight.xlThin;

                for (int indice = 0; indice < p_listeTemps.Count; indice++)
                {
                    feuilleCalcul.Cells[4 + indice, 1] = p_listeTemps[indice].Item1;
                    feuilleCalcul.Cells[4 + indice, 2] = p_listeTemps[indice].Item2.ElapsedTicks.ToString();
                }

                Excel._Chart graphique = (Excel._Chart)classeur.Charts.Add(Missing.Value, Missing.Value, Missing.Value, Missing.Value);
                Excel.Range gamme = feuilleCalcul.get_Range("A4", "B43");

                graphique.ChartWizard(gamme,
                                      Excel.XlChartType.xlXYScatterSmooth,
                                      1,
                                      Excel.XlRowCol.xlColumns,
                                      1,
                                      Missing.Value,
                                      Missing.Value,
                                      "Suite de Fibonacci: Temps en Ticks selon le nombre",
                                      "Nombre",
                                      "Temps (Ticks)",
                                      Missing.Value);

                graphique.ChartTitle.Font.Bold = true;
                graphique.ChartTitle.Font.Size = 14;

                graphique.Location(Excel.XlChartLocation.xlLocationAsObject, feuilleCalcul.Name);

                gamme = (Excel.Range)feuilleCalcul.Rows.get_Item(5, Missing.Value);
                feuilleCalcul.Shapes.Item("Chart 1").Top = (float)(double)gamme.Top;

                gamme = (Excel.Range)feuilleCalcul.Columns.get_Item(4, Missing.Value);
                feuilleCalcul.Shapes.Item("Chart 1").Left = (float)(double)gamme.Left;

                feuilleCalcul.Shapes.Item("Chart 1").Height = 400;
                feuilleCalcul.Shapes.Item("Chart 1").Width = 650;

                classeur.SaveAs2(cheminAbsoluFichier);
            }
        }
        #endregion

    }
}
