﻿using System;
using System.Collections.Generic;

namespace Recursivite_Bibliotheque
{
    public class FonctionsRecursives
    {
        #region "Compter en négatif"

        // Écrivez une première fonction récursive qui affiche les nombres de -n à 0 à l'écran.
        public static void AfficherNombresNegatifsCroissant(int p_nombre)
        {
            if(p_nombre > 0)
            {
                throw new ArgumentException("La valeur ne doit pas être supérieure à 0", nameof(p_nombre));
            }

            AfficherNombresNegatifsCroissant_rec(p_nombre, 0);
        }
        private static void AfficherNombresNegatifsCroissant_rec(int p_nombreDepart, int p_zero)
        {
            Console.Out.WriteLine(p_nombreDepart);

            if(p_nombreDepart < p_zero)
            {
                AfficherNombresNegatifsCroissant_rec(p_nombreDepart + 1, p_zero);
            }
        }
        // Écrivez une seconde fonction récursive qui affiche les nombres de 0 à -n.
        // Si vous avez utilisé plus d'une paramètre, écrivez une variante à un paramètre.
        public static void AfficherNombresNégatifsDecroissant(int p_nombre)
        {
            if (p_nombre > 0)
            {
                throw new ArgumentException("La valeur ne doit pas être supérieure à 0", nameof(p_nombre));
            }

            AfficherNombresNégatifsDecroissant_2(0, p_nombre);
        }
        private static void AfficherNombresNégatifsDecroissant_2(int p_nombreDepart, int p_nombreArrivee)
        {
            Console.Out.WriteLine(p_nombreDepart);

            if(p_nombreDepart > p_nombreArrivee)
            {
                AfficherNombresNégatifsDecroissant_2(p_nombreDepart - 1, p_nombreArrivee);
            }
        }
        #endregion

        #region "Division entière"

        //Écrivez une fonction récursive qui calcule le reste de la division entière de deux nombres entiers sans utiliser le modulo.
        public static int CalculerResteDivisionEntiere(int p_premierNombre, int p_deuxiemeNombre)
        {
            if(p_premierNombre < p_deuxiemeNombre)
            {
                throw new ArgumentException("Le premier nombre ne peut pas être plus petit que le deuxieme", nameof(p_premierNombre));
            }
            if(p_deuxiemeNombre == 0)
            {
                throw new ArgumentException("On ne peut pas diviser par zéro", nameof(p_deuxiemeNombre));
            }

            return CalculerResteDivisionEntiere_rec(p_premierNombre, p_deuxiemeNombre);
        }
        private static int CalculerResteDivisionEntiere_rec(int p_dividende, int p_diviseur)
        {
            int reste = p_dividende;

            if(p_dividende / p_diviseur != 0)
            {
                int quotient = (p_dividende / p_diviseur);
                reste = p_dividende - (quotient * p_diviseur);

                return CalculerResteDivisionEntiere_rec(reste, p_diviseur);
            }

            return reste;
        }

        #endregion

        #region "Suite de Fibonacci"

        // Question 1 : écrivez la fonction Fibonacci en version récursive qui calcule une valeur de la suite de Fibonacci.
        public static int CalculerValeurSuiteFibonacci(int p_nombre)
        {
            if(p_nombre < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_nombre));
            }

            return CalculerValeurSuiteFibonacci_rec(p_nombre);
        }
        private static int CalculerValeurSuiteFibonacci_rec(int p_nombre)
        {
            if(p_nombre > 1)
            {
                return CalculerValeurSuiteFibonacci_rec(p_nombre - 1) + CalculerValeurSuiteFibonacci_rec(p_nombre - 2);
            }

            return p_nombre;
        }

        /* Question 2 : quelle est la complexité de votre fonction ?

        - Cette fonction à une complexité O(n²).

        Question 3 : pourquoi la complexité est-elle aussi élevée ?

        - Car la fonction récursive s'appelle deux fois (se double) à chaque itération pour un nombre qui est supérieur à 1.

        Question 4 : comment pourriez-vous améliorez votre fonction ? Réécrivez une version optimisée.
        
        " En mathématiques, la suite de Fibonacci est une suite d'entiers dans laquelle chaque terme est la somme des deux termes
        qui le précèdent. "
        
         Source: https://fr.wikipedia.org/wiki/Suite_de_Fibonacci 
        
        
        Donc pour éviter de doubler la fonction récursive à chaque itération, on va parcourrir la suite de fibonacci comme si on le faisait avec une boucle
        for, on prend 4 paramètres, un indice, une limite (le nombre choisi), le nombre fibonacci en cours dans la suite et son suivant. À chaque récursion,
        (itération), on additionne le fibonacci en cours et son suivant. */
        public static int CalculerValeurSuiteFibonacciAmeliorer(int p_nombre)
        {
            if (p_nombre < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(p_nombre));
            }

            return CalculerValeurSuiteFibonacciAmeliorer_rec(0, p_nombre, 0, 1);
        }
        private static int CalculerValeurSuiteFibonacciAmeliorer_rec(int p_indice, int p_nombre, int p_fibonacciEncours, int p_fibonacciSuivant)
        {
            if (p_indice == p_nombre)
            {
                return p_fibonacciEncours;
            }

            p_indice++;

            return CalculerValeurSuiteFibonacciAmeliorer_rec(p_indice, p_nombre, p_fibonacciSuivant, p_fibonacciEncours + p_fibonacciSuivant);
        }
        #endregion

        #region "Liste chaînée"

        // Question 1 : écrivez une fonction récursive qui prend une liste chaînée générique et qui renvoie son nombre d'éléments en parcourant tous ses noeuds.
        public static int ObtenirNombreElementsListeChainee <TypeElement> (LinkedList<TypeElement> p_listeChainee)
        {
            if(p_listeChainee == null)
            {
                throw new ArgumentNullException(nameof(p_listeChainee));
            }

            LinkedListNode<TypeElement> noeud = p_listeChainee.First;

            return ObtenirNombreElementsListeChainee_rec(noeud, 0);
        }
        private static int ObtenirNombreElementsListeChainee_rec <TypeElement> (LinkedListNode<TypeElement> p_noeud, int p_nombreElement)
        {
            if (p_noeud != null)
            {
                p_nombreElement++;
                p_noeud = p_noeud.Next;

                return ObtenirNombreElementsListeChainee_rec(p_noeud, p_nombreElement);
            }

            return p_nombreElement;
        }
        // Question 2 : écrivez une fonction récursive qui prend une liste chaînée générique et qui renvoie la valeur la plus grande.
        public static TypeElement ObtenirValeurPlusGrandeListeChainee<TypeElement>(LinkedList<TypeElement> p_listeChainee)
        where TypeElement : IComparable<TypeElement>
        {
            if (p_listeChainee == null)
            {
                throw new ArgumentNullException(nameof(p_listeChainee));
            }

            LinkedListNode<TypeElement> noeud = p_listeChainee.First;

            return ObtenirValeurPlusGrandeListeChainee_rec(noeud, noeud.Value);
        }
        private static TypeElement ObtenirValeurPlusGrandeListeChainee_rec<TypeElement>(LinkedListNode<TypeElement> p_noeud, TypeElement p_valeur)
        where TypeElement : IComparable<TypeElement>
        {
            if (p_noeud.Next != null)
            {
                if(p_valeur.CompareTo(p_noeud.Next.Value) < 0)
                {
                    p_valeur = p_noeud.Next.Value;
                }

                return ObtenirValeurPlusGrandeListeChainee_rec(p_noeud.Next, p_valeur);
            }

            return p_valeur;
        }
        /* Question 3 : écrivez une fonction récursive qui prend une liste chaînée générique et une fonction lambda qui ne renvoie rien(Action<TypeElement>)
        et qui applique la fonction lambda sur chacun des noeuds. */
        public static void AppliquerLambdaListeChainee <TypeElement>(LinkedList<TypeElement> p_listeChainee, Action<TypeElement> p_fonctionLambda)
        {
            if (p_listeChainee == null)
            {
                throw new ArgumentNullException(nameof(p_listeChainee));
            }

            LinkedListNode<TypeElement> noeud = p_listeChainee.First;

            AppliquerLambdaListeChainee_rec(noeud, p_fonctionLambda);
        }
        private static void AppliquerLambdaListeChainee_rec <TypeElement>(LinkedListNode<TypeElement> p_noeud, Action<TypeElement> p_fonctionLambda)
        {
            if(p_noeud != null)
            {
                p_fonctionLambda(p_noeud.Value);

                if(p_noeud.Next != null)
                {
                    AppliquerLambdaListeChainee_rec(p_noeud.Next, p_fonctionLambda);
                }

            }
        }

        // Question 4 : réécrivez une nouvelle version des fonctions NombreDElements et Maximum en utilisant la fonction de parcours.
        public static int ObtenirNombreElementsListeChaineeParcours<TypeElement>(LinkedList<TypeElement> p_listeChainee)
        {
            if (p_listeChainee == null)
            {
                throw new ArgumentNullException(nameof(p_listeChainee));
            }

            int nombreElement = 0;

            Action<TypeElement> compteur = TypeElement => nombreElement++;

            AppliquerLambdaListeChainee(p_listeChainee, compteur);

            return nombreElement;
        }
        public static TypeElement ObtenirValeurPlusGrandeListeChaineeParcours<TypeElement>(LinkedList<TypeElement> p_listeChainee)
        {
            if (p_listeChainee == null)
            {
                throw new ArgumentNullException(nameof(p_listeChainee));
            }

            TypeElement valeurPlusGrande = default;
            List<TypeElement> listeElement = new List<TypeElement>();

            Action<TypeElement> ajout = element => listeElement.Add(element);

            AppliquerLambdaListeChainee(p_listeChainee, ajout);
            listeElement.Sort();

            if(listeElement.Count != 0)
            {
                valeurPlusGrande = listeElement[listeElement.Count - 1];
            }

            return valeurPlusGrande;
        }
        #endregion

        #region "Recherche"

        /* Question 1 : écrivez une fonction de recherche récursive qui prend un "IEnumerable" comme collection, une fonction lambda en paramètres 
        et qui renvoie l'élément s'il est trouvé, la valeur par défaut sinon :

        - La fonction lambda prend une valeur en paramètres et renvoie vrai si c'est la valeur à trouver, faux sinon.
        - Votre fonction privée doit prendre un "IEnumerator" en paramètres ainsi que la fonction lambda. */

        public static TypeElement RechercherElementLambda <TypeElement>(IEnumerable<TypeElement> p_collection, TypeElement p_valeur, Func<TypeElement, TypeElement, bool> p_rechercheLambda)
            where TypeElement : IComparable<TypeElement>
        {
            if(p_collection == null)
            {
                throw new ArgumentNullException(nameof(p_collection));
            }

            IEnumerator<TypeElement> enumerateur = p_collection.GetEnumerator();

            if (enumerateur.MoveNext() != false)
            {
                enumerateur.MoveNext();
            }

            return RechercherElementLambda_rec(enumerateur, p_valeur, p_rechercheLambda);
        }
        private static TypeElement RechercherElementLambda_rec <TypeElement>(IEnumerator<TypeElement> p_enumerateur, TypeElement p_valeur, Func<TypeElement, TypeElement, bool> p_rechercheLambda)
            where TypeElement : IComparable<TypeElement>
        {
            if (p_rechercheLambda(p_enumerateur.Current, p_valeur) != true)
            {
                if(p_enumerateur.MoveNext() != false)
                {
                    p_enumerateur.MoveNext();

                    return RechercherElementLambda_rec(p_enumerateur, p_valeur, p_rechercheLambda);
                }
                else
                {
                    return default;
                }
            }

            return p_enumerateur.Current;
        }
        /* Question 2 : écrivez une fonction récursive qui permet de compter le nombre d'éléments qui correspondent au critère d'une fonction lambda passée en paramètres :

        - La fonction lambda prend une valeur en paramètres et renvoie vrai si c'est la valeur à trouver, faux sinon.
        - Votre fonction privée doit prendre un "IEnumerator" en paramètres ainsi que la fonction lambda.
        - Votre fonction renvoie le nombre d'occurrences */
        public static int RechercherNombreElementsLambda<TypeElement>(IEnumerable<TypeElement> p_collection, TypeElement p_valeur, Func<TypeElement, TypeElement, bool> p_rechercheLambda)
            where TypeElement : IComparable<TypeElement>
        {
            if (p_collection == null)
            {
                throw new ArgumentNullException(nameof(p_collection));
            }

            int occurences = 0;
            IEnumerator<TypeElement>  enumerateur = p_collection.GetEnumerator();

            if (enumerateur.MoveNext() != false)
            {
                enumerateur.MoveNext();
            }

            return RechercherNombreElementsLambda_rec(enumerateur, p_valeur, p_rechercheLambda, occurences);
        }
        private static int RechercherNombreElementsLambda_rec<TypeElement>(IEnumerator<TypeElement> p_enumerateur, TypeElement p_valeur, Func<TypeElement, TypeElement, bool> p_rechercheLambda, int p_occurences)
            where TypeElement : IComparable<TypeElement>
        {
            if (p_rechercheLambda(p_enumerateur.Current, p_valeur) == true)
            {
                p_occurences++;
            }

            if (p_enumerateur.MoveNext() != false)
            {
                p_enumerateur.MoveNext();

                return RechercherNombreElementsLambda_rec(p_enumerateur, p_valeur, p_rechercheLambda, p_occurences);
            }

            return p_occurences;
        }

        /* Question 3 : écrivez une fonction récursive qui permet de filtrer une collection d'éléments qui correspondent au critère d'une fonction lambda passée en paramètres :

        - La fonction lambda prend une valeur en paramètres et renvoie vrai si c'est la valeur à trouver, faux sinon.
        - Votre fonction privée doit prendre un "IEnumerator" en paramètres ainsi que la fonction lambda.
        - Votre fonction renvoie une liste d'éléments du même type que les éléments de l'énumérateur. */
        public static List<TypeElement> FiltrerElementsLambda <TypeElement>(IEnumerable<TypeElement> p_collection, Func<TypeElement, bool> p_rechercheLambda)
            where TypeElement : IComparable<TypeElement>
        {
            if (p_collection == null)
            {
                throw new ArgumentNullException(nameof(p_collection));
            }

            List<TypeElement> liste = new List<TypeElement>();
            IEnumerator<TypeElement> enumerateur = p_collection.GetEnumerator();

            enumerateur.MoveNext();
            return FiltrerElementsLambda_rec(enumerateur, p_rechercheLambda, liste);
        }
        private static List<TypeElement> FiltrerElementsLambda_rec<TypeElement>(IEnumerator<TypeElement> p_enumerateur, Func<TypeElement, bool> p_rechercheLambda, List<TypeElement> p_liste)
            where TypeElement : IComparable<TypeElement>
        {
            if (p_rechercheLambda(p_enumerateur.Current) == true)
            {
                p_liste.Add(p_enumerateur.Current);
            }

            if (p_enumerateur.MoveNext() != false)
            {
                return FiltrerElementsLambda_rec(p_enumerateur, p_rechercheLambda, p_liste);
            }

            return p_liste;
        }
        #endregion

        #region "Recherche dichotomique"

        /* Écrivez une fonction de recherche dichotomique récursive qui prend un tableau d'éléments,
        une fonction lambda en paramètres et qui renvoie l'élément s'il est trouvé, la valeur par défaut sinon :

        La fonction lambda prend une valeur en paramètres et renvoie un entier correspondant à sa position par rapport à la valeur cherchée.
        Votre fonction privée doit prendre un tableau, les indices de début et de fin du sous-tableau ainsi que la fonction lambda. */
        public static TypeElement RechercheDichotomiqueLambda <TypeElement>(TypeElement[] p_tableau, Func<TypeElement, int> p_lambdaRechercheDichotomique)
        {
            if(p_tableau == null || p_tableau.Length == 0)
            {
                throw new ArgumentException(nameof(p_tableau));
            }

            int indiceDebut = 0;
            int indiceFin = p_tableau.Length - 1;

            return RechercheDichotomiqueLambda_rec(p_tableau, indiceDebut, indiceFin, p_lambdaRechercheDichotomique);
        }
        private static TypeElement RechercheDichotomiqueLambda_rec<TypeElement>(TypeElement[] p_tableau, int p_indiceDebut, int p_indiceFin, Func<TypeElement, int> p_lambdaRechercheDichotomique)
        {
            int indiceRecherche = p_indiceDebut + ((p_indiceFin - p_indiceDebut) / 2);

            if (indiceRecherche == p_indiceDebut && p_lambdaRechercheDichotomique(p_tableau[indiceRecherche]) != 0)
            {
                indiceRecherche++;

                if(indiceRecherche == p_indiceFin && p_lambdaRechercheDichotomique(p_tableau[indiceRecherche]) == 0)
                {
                    return p_tableau[indiceRecherche];
                }

                return default;
            }
            else if (p_lambdaRechercheDichotomique(p_tableau[indiceRecherche]) > 0)
            {
                p_indiceFin = indiceRecherche;
            }
            else if (p_lambdaRechercheDichotomique(p_tableau[indiceRecherche]) < 0)
            {
                p_indiceDebut = indiceRecherche; 
            }
            else if (p_lambdaRechercheDichotomique(p_tableau[indiceRecherche]) == 0)
            {
                return p_tableau[indiceRecherche];
            }

            return RechercheDichotomiqueLambda_rec(p_tableau, p_indiceDebut, p_indiceFin, p_lambdaRechercheDichotomique);
        }
        #endregion
    }
}
